from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm

class registerForm(UserCreationForm):

    def save(self, commit=True):
        user = super(registerForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.is_staff = True
        if commit:
            user.save()
        return user