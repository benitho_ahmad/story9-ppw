from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import registerForm
from django.http import JsonResponse
import requests
import json

def home(request):
    return render(request, 'main/home.html')

def register(response):
    if response.method == "POST":
        form = registerForm(response.POST)
        if form.is_valid():
            form.save()
            return redirect("/")
    else:
        form = registerForm()

    return render(response, "main/register.html", {"form":form})

def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    print(ret.content)
    data = json.loads(ret.content)

    if not request.user.is_authenticated:
        x = 0
        for i in data['items']:
            del data['items'][x]['volumeInfo']['imageLinks']['smallThumbnail']
            x = x + 1

    return JsonResponse(data, safe=False)